use std::net::{SocketAddr, IpAddr, Ipv4Addr};
use std::path::{PathBuf, Path};
use std::sync::Arc;

use clap::Parser;
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Method, Request, Response, Result, Server, StatusCode};
use tokio::fs::File;
use tokio_util::codec::{BytesCodec, FramedRead};

fn not_found() -> Response<Body> {
    Response::builder()
        .status(StatusCode::NOT_FOUND)
        .body("not found".into())
        .unwrap()
}

async fn file_response(path: &Path) -> Result<Response<Body>> {
    match File::open(path).await {
        Ok(file) => {
            let stream = FramedRead::new(file, BytesCodec::new());
            let body = Body::wrap_stream(stream);
            return Ok(Response::new(body));
        },
        Err(e) => { 
            eprintln!("Error: {} ('{}')", e.kind(), path.display());
            Ok(not_found())
        },
    }
}

// TODO: there seems to be no reason why it couldn't just be Path, need help
async fn serve(dir: Arc<PathBuf>, request: Request<Body>) -> Result<Response<Body>> {
    let dir = dir.as_path();

    match request.method() {
        &Method::GET => {
            let mut path = request.uri().path().to_string();
            if path.ends_with("/") {
                path += "index.html";
            }
            let path = path.split_at(1).1;
            let path = dir.join(path);
            file_response(&path).await
        },
        _ => Ok(not_found()),
    }
}

async fn shutdown_signal() {
    tokio::signal::ctrl_c()
        .await
        .expect("failed to install CTRL+C signal handler");
}

#[derive(Parser, Debug)]
#[command()]
struct Opts {
    #[clap(short, long, help="bind to this address (default: 127.0.0.1:8080)")]
    pub bind: Option<SocketAddr>,
    
    #[clap(short, long, help="root directory (default: current directory)")]
    pub dir: Option<PathBuf>,
}

#[tokio::main]
async fn main() -> std::result::Result<(), Box<dyn std::error::Error>> {
    let Opts { bind, dir } = Opts::parse();
    let bind = bind.unwrap_or_else(|| SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 8080));
    let dir = dir.unwrap_or_else(|| std::env::current_dir().expect("couldn't get current dir"));
    let dir = Arc::new(dir);

    let make_service = make_service_fn(|_| {
        let dir = dir.clone();
        async move {
            Ok::<_, hyper::Error>(service_fn(move |req| {
                serve(dir.clone(), req)
            }))
        }
    });

    let server = Server::bind(&bind).serve(make_service);
    let server = server.with_graceful_shutdown(shutdown_signal());
    println!("Listening on http://{}", bind);

    if let Err(e) = server.await {
        eprintln!("server error: {}", e);
    }

    Ok(())
}

